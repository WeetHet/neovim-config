local function apply(name, values)
    for k, v in pairs(values) do
        vim[name][k] = v
    end
end

apply("opt", require("vim_config.opts"))
apply("wo", require("vim_config.wos"))
apply("g", require("vim_config.globals"))
