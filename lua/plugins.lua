local ensure_lazy = function()
    local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system({
            'git',
            'clone',
            '--filter=blob:none',
            'https://github.com/folke/lazy.nvim.git',
            '--branch=stable', -- latest stable release
            lazypath,
        })
        return true
    end
    vim.opt.rtp:prepend(lazypath)
    return false
end

local lazy_bootstrap = ensure_lazy()

local lazysetup = require('lazy').setup({
    'sainnhe/edge',
    'j-hui/fidget.nvim',

    require('setup.treesitter'),
    require('setup.telescope'),
    require('setup.alpha'),
    require('setup.barbar'),
    require('setup.minipairs'),
    require('setup.lualine'),

    require('setup.transparent'),

    "L3MON4D3/LuaSnip",
    require('setup.lsp'),
    require('setup.cmp'),

    require('setup.ibl'),
    require('setup.surround'),
    require('setup.which-key'),
    'echasnovski/mini.comment',
    require('setup.fidget')
}, { lockfile = vim.fn.stdpath('data') .. '/lazy/lockfile' })

if lazy_bootstrap then
    require('lazy').sync()
end

return lazysetup
