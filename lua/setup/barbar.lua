local function generate_keys()
    local keys = {
        { '<Tab>',      function() vim.cmd.BufferNext() end,     desc = 'Go to next buffer' },
        { '<S-Tab>',    function() vim.cmd.BufferPrevious() end, desc = 'Go to previous buffer' },
        { '<leader>tc', function() vim.cmd.BufferDelete() end,   desc = 'Close current tab' }
    }
    for t = 1, 9 do
        table.insert(keys, {
            '<leader>t' .. t,
            '<cmd> BufferGoto ' .. t .. ' <CR>',
            desc = 'Go to buffer ' .. t,
            silent = true
        })
    end

    return keys
end

return {
    'romgrk/barbar.nvim',
    lazy = false,
    keys = generate_keys(),
    init = function() vim.g.barbar_auto_setup = true end,
}
