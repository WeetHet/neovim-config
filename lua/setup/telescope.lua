local telescope = nil
local builtin = nil

local function lazy_telescope()
    if telescope == nil then
        telescope = require('telescope')
    end
    return telescope
end

local function lazy_builtin()
    if builtin == nil then
        builtin = require('telescope.builtin')
    end
    return builtin
end

return {
    'nvim-telescope/telescope.nvim',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'jvgrootveld/telescope-zoxide',
        'nvim-lua/popup.nvim',
        'stevearc/aerial.nvim'
    },
    keys = {
        { '<leader>fb', function() lazy_builtin().buffers() end,                    desc = 'Telescope list buffers' },
        { '<leader>ff', function() lazy_builtin().find_files() end,                 desc = 'Telescope find files' },
        { '<leader>fg', function() lazy_builtin().live_grep() end,                  desc = 'Telescope live grep' },
        { '<leader>fh', function() lazy_builtin().oldfiles() end,                   desc = 'Telescope oldfiles' },
        { '<leader>ae', function() lazy_telescope().extensions.aerial.aerial() end, desc = 'Telescope aerial' },
        {
            '<leader>fz',
            function() lazy_telescope().extensions.zoxide.list() end,
            desc = 'Telescope list files via zoxide'
        },
    },
    config = function()
        local z_utils = require('telescope._extensions.zoxide.utils')
        lazy_telescope().load_extension('aerial')

        lazy_telescope().setup {
            defaults = {
                mappings = {
                    n = {
                        e = 'move_selection_next', i = 'move_selection_previous',
                        N = 'move_to_top', O = 'move_to_bottom',
                        H = 'nop', k = 'nop', j = 'nop', L = 'nop'
                    }
                }
            },
            extensions = {
                aerial = {},
                possession = {},
                zoxide = {
                    prompt_title = '[ Walking on the shoulders of TJ ]',
                    mappings = {
                        default = {
                            after_action = function(selection)
                                print('Update to (' .. selection.z_score .. ') ' .. selection.path)
                            end
                        },
                        ['<C-s>'] = {
                            before_action = function(selection) print('before C-s') end,
                            action = function(selection)
                                vim.cmd.edit(selection.path)
                            end
                        },
                        ['<C-q>'] = { action = z_utils.create_basic_command('split') },
                    },
                }
            }
        }
    end
}
