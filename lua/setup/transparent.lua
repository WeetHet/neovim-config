local function generate_groups()
    local status = {
        "Alternate", "Current", "Inactive", "Visible"
    }

    local part = {
        "ADDED", "CHANGED", "DELETED", "ERROR", "HINT", "Icon", "Index",
        "INFO", "Mod", "Number", "Sign", "SignRight", "Target", "WARN"
    }

    local barbar_groups = {
        "BufferTabpageFill", "BufferTabpages", "TabLine", "TabLineSel", "TabLineFill", "NormalFloat", "FloatBorder"
    }

    for _, s in ipairs(status) do
        for _, p in ipairs(part) do
            table.insert(barbar_groups, 'Buffer' .. s .. p)
        end
    end

    return barbar_groups
end

return {
    'xiyaowong/transparent.nvim',
    config = function()
        require("transparent").setup({
            extra_groups = generate_groups()
        })
    end
}
