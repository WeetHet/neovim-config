local ruff_on_attach = function(client, _)
    client.server_capabilities.hoverProvider = false
end

return {
    on_attach = ruff_on_attach,
}
