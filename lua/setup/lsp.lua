local function enable_rounded_borders()
    vim.diagnostic.config({
        virtual_text = false,
        severity_sort = true,
        float = {
            border = 'rounded',
            source = 'always',
        },
    })

    vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
        vim.lsp.handlers.hover,
        { border = 'rounded' }
    )

    vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
        vim.lsp.handlers.signature_help,
        { border = 'rounded' }
    )
end

local function extend_with_cmp(lsp_defaults)
    lsp_defaults.capabilities = vim.tbl_deep_extend(
        'force',
        lsp_defaults.capabilities,
        require('cmp_nvim_lsp').default_capabilities()
    )
end

local function on_attach(ev)
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', '<leader>k', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>cf', function()
        vim.lsp.buf.format { async = true }
    end, opts)
end

local function setup_signs()
    local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
    for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end
end

return {
    'neovim/nvim-lspconfig',
    dependencies = { 'hrsh7th/nvim-cmp' },
    keys = {
        { "<leader>e",  vim.diagnostic.open_float, "Open diagnostics" },
        { '[',          vim.diagnostic.goto_prev,  "Go to previous diagnostic" },
        { ']',          vim.diagnostic.goto_next,  "Go to next diagnostic" },
        { '<leader>do', vim.diagnostic.setloclist, "Open diagnostic list" }
    },
    config = function()
        enable_rounded_borders()

        local lspconfig = require('lspconfig')
        local lsp_defaults = lspconfig.util.default_config

        extend_with_cmp(lsp_defaults)

        lspconfig.tinymist.setup {}
        lspconfig.pyright.setup {}
        lspconfig.zls.setup {}
        lspconfig.html.setup {}
        lspconfig.rust_analyzer.setup(require('setup.lsp_servers.rust_analyzer'))
        lspconfig.gopls.setup(require('setup.lsp_servers.gopls'))
        lspconfig.lua_ls.setup(require('setup.lsp_servers.luals'))
        lspconfig.clangd.setup {}
        lspconfig.metals.setup {}
        lspconfig.ruff_lsp.setup(require('setup.lsp_servers.ruff'))

        vim.api.nvim_create_autocmd('LspAttach', {
            group = vim.api.nvim_create_augroup('UserLspConfig', {}),
            callback = on_attach,
        })

        setup_signs()
    end
}
