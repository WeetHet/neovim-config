return {
    'goolord/alpha-nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    lazy = false,
    keys = {
        { "<leader>h", function() vim.cmd.Alpha() end }
    },
    config = function()
        local alpha = require("alpha")

        local function footer()
            local datetime = os.date("  %m-%d-%Y   %H:%M:%S")
            local version = vim.version()
            local nvim_version_info = "   v" .. version.major .. "." .. version.minor .. "." .. version.patch
            return datetime .. nvim_version_info
        end

        local dashboard = require("alpha.themes.dashboard")
        dashboard.section.header.val = require("setup.alpha_headers.neovim")

        dashboard.section.buttons.val = {
            dashboard.button("<leader> e n", "   New File", ":ene <BAR> startinsert <CR>"),
            dashboard.button("<leader> f h", "   Recently Files", ":Telescope oldfiles <CR>"),
            dashboard.button("<leader> f t", "   Find Text", ":Telescope live_grep <CR>"),
            dashboard.button("<leader> s p", "   Sync Plugins", ":Lazy sync <CR>"),
            dashboard.button("<leader> i p", "   Add/Remove Plugins", ":e ~/.config/nvim/lua/plugins.lua<CR>"),
            dashboard.button("<leader>   c", " X  Quit Neovim", ":qa<CR>"),
        }

        dashboard.section.footer.val = footer()
        dashboard.opts.opts.noautocmd = true
        alpha.setup(dashboard.opts)
    end
}
