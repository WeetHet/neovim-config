local remap = {
    [{ 'n', 'v' }] = {
        { key = "n",         action = "h",    desc = "left" },
        { key = "e",         action = "j",    desc = "down" },
        { key = "i",         action = "k",    desc = "up" },
        { key = "o",         action = "l",    desc = "right" },
        { key = "I",         action = "K",    desc = "docs" },
        { key = "l",         action = "^",    desc = "start of line" },
        { key = "L",         action = "$",    desc = "end of line" },
        { key = "N",         action = "H",    desc = "first (n-th) line" },
        { key = "O",         action = "L",    desc = "last (n-th) line" },
        { key = "<leader>p", action = "\"+p", desc = "paste from clipboard" },
        { key = "<leader>y", action = "\"+y", desc = "yank to clipboard" },
        { key = "<leader>d", action = "\"+d", desc = "delete to clipboard" },
        { key = "f",         action = "e",    desc = "forward to end of word" },
        { key = "F",         action = "E",    desc = "forward to end of WORD" },
        { key = "m",         action = "f",    desc = "move to next character" },
        { key = "M",         action = "F",    desc = "move to previous character" },
        { key = "E",         action = "",     desc = "disabled" }
    },
    v = {
        { key = "h", action = "",  desc = "disabled" },
        { key = "j", action = "",  desc = "disabled" },
        { key = "k", action = "i", desc = "inside but k" }
    },
    n = {
        { key = "<ESC>",      action = "<cmd> noh <CR>",   desc = "clear search highlight" },
        { key = "j",          action = "n",                desc = "next word search" },
        { key = "J",          action = "N",                desc = "prev word search" },
        { key = "k",          action = "i",                desc = "insert mode" },
        { key = "h",          action = "o",                desc = "line before + insert" },
        { key = "H",          action = "O",                desc = "line after + insert" },
        { key = "dkw",        action = "diw",              desc = "delete (k)inner word" },
        { key = "<leader>h",  action = "<cmd> Alpha <CR>", desc = "toggle case" },
        { key = "<leader>qq", action = "<cmd> qa <CR>",    desc = "quit all" },
        { key = "<leader>q!", action = "<cmd> q! <CR>",    desc = "quit without saving" },
        { key = "<leader>wq", action = "<cmd> wq <CR>",    desc = "write and quit" },
        { key = "gch",        action = "gco",              desc = "Comment.nvim" },
        { key = "gcH",        action = "gcO",              desc = "Comment.nvim" },
        { key = "gcn",        action = "gch",              desc = "Comment.nvim" },
        { key = "gcN",        action = "gcH",              desc = "Comment.nvim" },
        { key = "gce",        action = "gcj",              desc = "Comment.nvim" },
        { key = "gci",        action = "gck",              desc = "Comment.nvim" },
        { key = "<C-w>n",     action = "<C-w>h",           desc = "move to left window" },
        { key = "<C-w>e",     action = "<C-w>j",           desc = "move to down window" },
        { key = "<C-w>i",     action = "<C-w>k",           desc = "move to up window" },
        { key = "<C-w>o",     action = "<C-w>l",           desc = "move to right window" },
        { key = "<C-w>w",     action = "<C-w>n",           desc = "move to next window" },
    },
}

for mode, binds in pairs(remap) do
    for _, bind in ipairs(binds) do
        vim.keymap.set(
            mode,
            bind.key,
            bind.action,
            { desc = bind.desc }
        )
    end
end
